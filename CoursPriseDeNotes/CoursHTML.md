# Notes de cours

1. [HTML.](#introduction)
   1. [Les balises principales.](#Balprinc)
   2. [Les balises du corps](#Body)
   3. [a finir](#finir)
2. [CSS.](#fin)

## HTML <a id="introduction"></a>
Le HTML est un language de balisage.  
HTML (Hyper Text Markup Language).

## I. Les Balises principales. <a id="Balprinc"></a>

<**!DOCTYPE**>, dit au navigateur qu'il va lire du HTML5, à mettre en premier.

```html
<!DOCTYPE html>
```

2) <**html**> Elle déclare le début du document, tout le code sera écris entre ces balises.

```html
<!DOCTYPE html>
<html lang="fr">
--code de la page--
</html>
```

-**lang** est l'attribut pour spécifier la langue de la page.  
-**fr** est la proprièté, pour dire que la page est en français.

3) <**head**> balise d'en-tête, on y met les métadonnées, elles ne seront pas visible sur le site.

```html
<!DOCTYPE html>
<html lang="fr">
    <head>
        métadonnés
    </head>
</html>
```

4) **Indentation**, de manière générale on décale vers la droite les balises qui se trouve entre d'autres balises, pour
   une meilleurs visibilité.


5) **title**, définit le titre de la page et sera afficher dans l'onglet du navigateur, très utile pour le référencement.
```html
<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>Nom de la page</title>
    </head>
</html>
```
6) **Les métadonnées dans l'en-tête.**  
   l'attribut **charset**, définit l'encodage du document, en général on met (utf-8).
```html
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8"/>
    <title>Nom de la page</title>
</head>
</html>
```  
-La balise <**meta**/> est auto-fermante.


## II. Les balises du corps <a id="Body"></a>

1) **body.**

2) **Commentaire.**

3) **Les titres, il éxiste 6 niveaux de h1 à h6, h1 est le plus important**

```html
<h1>Titre de Niveau1 Important</h1>
```




### III. à finir <a id="finir"></a>


## IV.fin <a id="fin"></a>




